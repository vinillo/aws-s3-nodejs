
/**
 * GET /
 * Home page.
 */
const AWS = require('aws-sdk');
const jsdom = require('jsdom');

const { JSDOM } = jsdom;
const { window } = new JSDOM();
const { document } = (new JSDOM('')).window;
global.document = document;

const $ = jQuery = require('jquery')(window);
const Videos = require('../models/Videos');

const bucketName = 'vinillobucket';
const s3Region = 'eu-central-1';

let objVideos = [];

// sort on key values
function keysrt(key, desc) {
  return function (a, b) {
    return desc ? ~~(a[key] < b[key]) : ~~(a[key] > b[key]);
  };
}


exports.get = (req, res) => {
  Videos.deleteMany({}, (err, result) => {
    if (result) {
      console.log('delete all');
    }
    if (err) {
      console.log('delete all error');
    }
  });

  AWS.config.loadFromPath('./config/aws.json');
  const s3 = new AWS.S3();
  const params = {
    Bucket: bucketName
  };
  s3.listObjects(params, (err, data) => {
    if (err) throw err;
    const obj = data.Contents.sort(keysrt('LastModified','asc'));
    obj.forEach((element) => {
      $.each(element, (index, value) => {
        if (index === 'Key') {
          objVideos.push(value);
          const saveVideo = new Videos({
            key: element.Key,
            last_modified: element.LastModified,
            bucket: bucketName,
            region: s3Region,
          });
          saveVideo.save((err) => {
            if (err) {
              console.log('save error');
            }
          });
        }
      });
    });
    res.write(JSON.stringify(objVideos), () => { objVideos = []; res.end(); });
  });
};


exports.refresh = (req, res) => {
  let amount = [];
  Videos.countDocuments({}, (err, result) => {
    if (err) { return '{}'; }
    if (result) {
      amount = result;
      console.log('from db');
      console.log(result);
    }
    if (err) {
      amount = 0;
      console.log(err);
    }
    res.write(JSON.stringify(amount), () => { amount = []; res.end(); });
  });
};
