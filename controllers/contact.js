const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  service: 'SendGrid',
  auth: {
    user: process.env.SENDGRID_USER,
    pass: process.env.SENDGRID_PASSWORD
  }
});
const s3Region = 'eu-central-1';
const bucketName = 'vinillobucket';

/**
 * POST /send
 * Send a contact form via Nodemailer.
 */
exports.send = (req, res) => {
  const fromName = 'Contact | nodeJs app';
  req.assert('email', 'Email is not valid').isEmail();


  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect(`/video/${req.body.movie}`);
  }

  const fromEmail = req.body.email;
  const title = req.body.movie;

  const message = `https://s3.${s3Region}.amazonaws.com/${bucketName}/${encodeURIComponent(title)}`;

  const mailOptions = {
    to: req.body.email,
    from: `${fromName} <${fromEmail}>`,
    subject: `${title} | Webkleur`,
    text: `Sup developer, check out this video: ${message}`,
  };

  transporter.sendMail(mailOptions, (err) => {
    if (err) {
      req.flash('errors', { msg: err.message });
      return res.redirect(`/video/${title}`);
    }
    req.flash('success', { msg: 'Email has been sent successfully!' });
    res.redirect(`/video/${title}`);
  });
};
