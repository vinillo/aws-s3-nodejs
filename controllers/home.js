/**
 * GET /
 * Home page.
 */
const jsdom = require('jsdom');

const { JSDOM } = jsdom;
const { window } = new JSDOM();
const { document } = (new JSDOM('')).window;
global.document = document;

const $ = jQuery = require('jquery')(window);

exports.index = (req, res) => {
  res.render('home', {
    title: 'Home',
  });
};
