/**
 * GET /
 * video page.
 */
const jsdom = require('jsdom');

const { JSDOM } = jsdom;
const { window } = new JSDOM();
const { document } = (new JSDOM('')).window;
global.document = document;

const $ = jQuery = require('jquery')(window);

const bucketName = 'vinillobucket';
const s3Region = 'eu-central-1';


exports.index = (req, res) => {
  const { video } = req.params;
  res.render('video', {
    title: video,
    movie: video,
    bucket: bucketName,
    region: s3Region,
  });
};
