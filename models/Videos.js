const mongoose = require('mongoose');

const videosSchema = new mongoose.Schema({
  key: { type: String },
  last_modified: { type: String },
  bucket: { type: String },
  region: { type: String },

}, { timestamps: true });


const Videos = mongoose.model('Videos', videosSchema);

module.exports = Videos;
